# Other Resources

This page has some links to other people who have put together some awesome information on this case.

> [JDM_WAAAT 'watercooled server' YouTube Playlist](https://www.youtube.com/watch?v=54WkXcjUf80&list=PLahxXMkkX6cBzh11N2BS3RH0y-c1qnlYi)
>
>> This is an awesome series of 5 videos in which JDM_WAAAT upgrades the computer in this case to dual water cooled Xeons instead of his one water cooled GPU from his previous build. This is where I got the idea to try and water cool the ThreadRipper 3970X for the TRminator build I'm planning.
