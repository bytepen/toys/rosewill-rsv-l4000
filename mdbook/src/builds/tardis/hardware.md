# Hardware

This is an overview of the hardware of my TARDIS build.

## CPU: i7-4790k

I bought this CPU via Newegg on 01DEC2014 for $299.99.

## Motherboard: MSI z97-GD65

I bought this motherboard via Newegg on 01DEC2014 for $139.99.

## RAM: Kingston HyperX FURY (32 GB)

I've had 32GB of [Kingston HyperX FURY](https://www.amazon.com/gp/product/B00J8E92I0/) for a long time. Its clock speed is 1866 MHz and is DDR3.

I purchased one stick on 01DEC2014 for $69.99. I purchased a second stick on 15FEB2015 for $68.99. I purchased a kit of two sticks on 15FEB2015 for $133.99. This brought me to the max of 32 GB supported by my motherboard for a total of $273.

## GPU: GTX970

The first GPU I bought for this build was the [GTX760](https://www.amazon.com/gp/product/B00DBPKEOI/) on 29DEC2019 for $219.99.

I bought a replacement GTX970 via [Amazon](https://www.amazon.com/gp/product/B00NVODXME/) on 14JUN2015 for $329.99 that I have been using ever since.

## PSU: Corsair CS550M

I bought this PSU via Newegg on 01DEC2014 for $59.99 and it has lasted me ever since.

## Storage

At first, I purchased a [240GB Kingston SSD](https://www.amazon.com/gp/product/B00A1ZTZNM/) for $78.99 on 01DEC2014 to serve as my OS drive for Windows.

Below is a list of drive currently in my system

> 3.5" HDD's
>
>> 1x [8TB WD Easystore (White)](https://www.bestbuy.com/site/wd-easystore-8tb-external-usb-3-0-hard-drive-black/5792401.p) (23APR2020 - $139.99)
>> 1x [8TB WD Easystore (White)](https://www.bestbuy.com/site/wd-easystore-8tb-external-usb-3-0-hard-drive-black/5792401.p) (25NOV2017 - $159.99)
>> 1x [8TB WD Easystore (Red)](https://www.bestbuy.com/site/wd-easystore-8tb-external-usb-3-0-hard-drive-black/5792401.p) (04JUL2017 - $199.99)
>> 2x [3TB WD Reds](https://www.amazon.com/Red-3TB-Internal-Hard-Drive/dp/B083XVD1FP) (22FEB2017 - $109.99/ea)
>> 1x [5TB Seagate Barracuda](https://www.amazon.com/Seagate-BarraCuda-Internal-2-5-Inch-ST5000LM000/dp/B01M0AADIX) (24MAR2015 - $159.99)
>> 1x [1TB WD Caviar Green](https://www.amazon.com/Digital-INTELLISTORE-Deskptop-1Terabyte-SATA2-SATA3/dp/B06XR3DDCB) (Old Build - Dated 04NOV2010)
>
> Removed
>
>> 1x [8TB WD Easystores (White)](https://www.bestbuy.com/site/wd-easystore-8tb-external-usb-3-0-hard-drive-black/5792401.p) (23APR2020 - $139.99 - Snapped SATA Data Pins. Returned to Easystore shell.)

> 2.5" HDD's
>
>> 1x [1TB WD Blue](https://www.amazon.com/WD-Notebook-Internal-5400RPM-WD10JPVX/dp/B01EMXO3DK/) (Old Laptop - 22JUL2012 - $109.99)

> 2.5" SSD's
>
>> 1x [240GB Kingston SSD](https://www.amazon.com/Kingston-Digital-SSDNow-SV300S37A-240G/dp/B00A1ZTZNM) (01DEC2014 - $78.99)
>> 1x [240GB Kingston SSD](https://www.amazon.com/Kingston-Digital-SSDNow-SV300S37A-240G/dp/B00A1ZTZNM) (Friend - Drive Dying)
>> 1x 256GB Crucial SSD (Friend)

> M.2
>
>> 1x [512GB Intel 600P](https://www.amazon.com/gp/product/B01JSJA65C/) (25APR2017 - $193.61)
>> 1x 512GB Hynix BC501 (Upgraded Laptop)
>> 1x [500GB WD Blue 3D NAND SATA](https://www.amazon.com/Blue-NAND-500GB-SSD-WDS500G2B0B/dp/B073SBX6TY/) (21APR2018 - $125.99)

