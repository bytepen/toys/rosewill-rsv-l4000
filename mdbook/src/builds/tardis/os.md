# OS

## Windows

I used Windows 7 from the time I build this computer until around 24MAR2015 when I bought a 5TB Seagate Barracuda for $163.98 and had issues getting it to work just right with Windows 7. I then updated to an early preview of Windows 10 and stuck with that until I eventually switched it over to Unraid on 30APR2018.

## Unraid

I've been running Unraid on TARDIS since 30APR2018 with very few issues. For quite some time I had a Windows VM taking up most of its resources. This VM had a GTX970 and PCI USB controller passed through to it, which I used as a daily driver. It worked very well, but I eventually bought a very nice laptop and stopped using it as much. Since then it's been serving a more traditional server role with various CLI VMs and docker containers.
