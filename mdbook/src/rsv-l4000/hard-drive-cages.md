# Hard Drive Cages

One of the things that I really like about this case is that the front is pretty customizable based on your needs. The stock configuration has a slot for 3 DVD drives and two enclosures that can hold 4 3.5" HDDs each. There is very little room between the hard drives and the middle fan partitions, meaning that you will have to take out 4 screws to remove the fan partition any time that you are making any changes. This also leaves you
with very little option for trying to pretty up the cables, which has led to this area between the fans and the hard drives typically ending up be
ing stuffed with cables.

One thing that can be done to make this situation a little better, in my opinion, is to switch the HDD cages with [Rosewill's Hot-Swap Hard Drive Cages](https://www.amazon.com/Rosewill-5-25-Inch-3-5-Inch-Hot-swap-SATAIII/dp/B00DGZ42SM/), unfortunately, these can be increasingly hard to find.
 I originally had a few DVD drives that I never used installed, but I removed them for one of these enclosures on 19JAN2019 for $52.99. It is well
 worth every penny. I think I didn't feel like spending $150 on hard drive enclosures when I saw them available, but I was kicking myself for a lo
ng time for not buying them when they were available because the next time I could find some was 30JUN2020, in which I found two more available on
 Amazon for $50.99 each and immediately snatched them up lest they become unfindable for another year and a half, if not more. No regrets as the f
ront of my case is now "complete."

The only downsides to this configuration that I've found are that there doesn't seem to be an easy way to mount 3 of the Hot-Swap Cages in the sam
e orientation. I tried many different orientations and the only way I found acceptable was to mount the left two vertically with the release buttons on the top, and the right one horizontally with the release button on the right. It doesn't look as good as it could, but all the other orienta
tions left massive gaps with no way to mount the power buttons and/or wiggly hard drive enclosures. In this configuration, everything is steadily
secured and it doesn't look too bad if you can get past the different drive orientations.
