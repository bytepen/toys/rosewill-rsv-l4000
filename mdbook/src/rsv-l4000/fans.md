# Fans

The fans that come with this case are generic cheap fans that I'm having trouble finding much information about. It comes with a total of 5 120mm (BOK BDM12025S) and 2 80mm(BOK BDM8025S) fans. They're very quiet, although I couldn't find much information about them in regards to air flow or
static pressure. It should be noted that they're also powered exclusively via molex, so there's no PWM. 

Three of the 120's are installed on a partition separating the HDD/disk drives from the motherboard area. Two of them are installed on the stock Hard Drive cages These all pull air from the front of the case to the back.

The two 80's are installed as exhause fans above the IO panel of the motherboard.

Although they're generic, I will say that they've been very quiet and keep the case cool enough for the most part. I have had some issues with an
NVMe drive on a PCIe riser getting hotter than I would like and I'm planning on upgrading to a ThreadRipper 3970X with my TRminator build, so I ha
ve decided to upgrade everything to Noctua NF-F12 102mm fans and Noctua NF-R8 80mm fans.
