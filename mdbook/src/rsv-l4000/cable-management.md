# Cable Management

My only real complaint about this case is that there are next to no options for cable management. I'm the kind of guy who would gladly spend hours
 figuring out the optimal path for cables, but since there's nowhere to hide them here, you're kind of out of luck. One good thing about a rackmou
nt solid metal enclosure is that nobody can see inside.
