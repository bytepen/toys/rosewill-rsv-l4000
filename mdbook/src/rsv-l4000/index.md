# RSV-L4000

I bought this case via [Amazon](https://www.amazon.com/gp/product/B0056OUTBK/) for $90.99 USD on 12JAN2017 because I wanted to consolidate my 3D printersi, network equipment, and desktop into a 42U server rack I somehow convinced my wife to let me move into the livingroom of our apartment. I don't know how I swung that!

Overall, the [Rosewill RSV-L4000](https://www.rosewill.com/product/rosewill-rsv-l4000-4u-rackmount-server-case-chassis-8-internal-bays-7-cooling-fans-included/) is a very solid case which has lasted me until the time of this writing (07JUL2020) and I have no plans on replacing it.

This page will essentialls serve as a TL;DR for aspects of the case that I feel deserve extra attention. You can find more information about these sections on their corresponding pages.

## Cable Management

My only real complaint about this case is that there are next to no options for cable management. I'm the kind of guy who would gladly spend hours figuring out the optimal path for cables, but since there's nowhere to hide them here, you're kind of out of luck. One good thing about a rackmount solid metal enclosure is that nobody can see inside.

## Fans

The fans that come with this case are generic cheap fans that I'm having trouble finding much information about. It comes with a total of 5 120mm (BOK BDM12025S) and 2 80mm(BOK BDM8025S) fans. They're very quiet, although I couldn't find much information about them in regards to air flow or static pressure. It should be noted that they're also powered exclusively via molex, so there's no PWM.

## Form-Factor

This 4U case is massive and heavy, measuring at 7.00"x16.80"x25.00". It looks like it should be able to hold an E-ATX board with no problem, but I've only ever tried an ATX board. It should be noted that this case can BARELY fit a [Cooler Master Hyper 212 Evo](https://www.amazon.com/Enfriador-compacto-tuber%C3%ADa-contacto-directo/dp/B005O65JXI/), which touches the lid when closed. No chance something like the [Noctua NH-U14S-TR4](https://www.amazon.com/Noctua-NH-U14S-TR4-SP3-Premium-Grade-Cooler/dp/B074DX2SX7) will fit, leaving your higher-end cooling solution options with just liquid-cooling.

## Hard Drive Cages

One of the things that I really like about this case is that the front is pretty customizable based on your needs. The stock configuration has a slot for 3 DVD drives and two enclosures that can hold 4 3.5" HDDs each.

Another option is [Rosewill's Hot-Swap Hard Drive Cages](https://www.amazon.com/Rosewill-5-25-Inch-3-5-Inch-Hot-swap-SATAIII/dp/B00DGZ42SM/). I really like these, but unfortunately, they are increasingly hard to find. 

## Rails

Rosewill does have some rails I purchased for via [Amazon](https://www.amazon.com/gp/product/B01D3FJ5HC/) on 14DEC2017 for $44.99. After having to fight them for several hours in order to get them to mount just right, I can say they're the hardest rails I've ever had to install. That said, with enough patience, they're perfectly solid. Considering I don't really know of any other rails for this case, I suppose I would recommend them since you're able to get the major annoyance out of the way once and use them indefinitely.
