# Form-Factor

As I'm sure you know by now, this case is a 4U rack mount server. Its dimensions are 7.00"x16.80"x25.00", making it quite a large case that should hold **most** of your components with ease. It looks like it can hold an E-ATX no problem, but I've only ever tried it with an ATX board.

One major thing to consider is that CPU heatsinks are getting quite large these days. I have had the [Cooler Master Hyper 212 Evo](https://www.amazon.com/Enfriador-compacto-tuber%C3%ADa-contacto-directo/dp/B005O65JXI/) with my TARDIS build since the beginning, but this case is BARELY too small for it. It does shut, but I'm sure it's probably putting some pressure in some less-than-optimal locations. With the TRminator build I'm planning using the HOT AMD ThreadRipper 3970X, there is no way a 212 Evo would be a good choice. While I would love to use the [Noctua NH-U14S-TR4](https://www.amazon.com/Noctua-NH-U14S-TR4-SP3-Premium-Grade-Cooler/dp/B074DX2SX7) with this build, there is not a chance in hell it would fit. With that, I'm planning to replace the fan partition with the [NZXT Kraken X73](https://www.amazon.com/NZXT-Kraken-X73-360mm-RL-KRX73-01/).

